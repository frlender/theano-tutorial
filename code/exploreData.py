# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 15:19:03 2016

@author: qiaonan
"""
import pickle, gzip, numpy


f = gzip.open('mnist.pkl.gz', 'rb')
train_set, valid_set, test_set = pickle.load(f,encoding='latin1')
f.close()